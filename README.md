# Home Server Docker Apps

Subfolders in the `apps` folder contain docker-compose 'apps' that ought to be running on the device.  Ideally the docker composition will mount volumes in their subdirectores, making backup of this system easier by simply backing up this entire repository.

Example: `apps/home-assistant` has `config` defined as a volume, so all the config/db will wind up in `apps/home-assistant/config`

## Running an app


For example: start home-assistant

    cd apps/home-assistant
    sudo docker-compose up -d