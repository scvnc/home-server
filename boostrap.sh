#!/bin/bash

set -e

USER=home-server
INSTALL_DIR=/opt/home-server

# any dependencies from apt?
sudo apt-get update
sudo apt-get install openssh-server tmux

# Make User
id -u ${USER} || useradd -g docker ${USER}

# Lock account
sudo passwd ${USER} -l

# Create Install Directory
sudo mkdir -p ${INSTALL_DIR}
sudo chown ${USER}:docker ${INSTALL_DIR}

# Clone directory
sudo -u ${USER} git clone https://gitlab.com/scvnc/home-server.git ${INSTALL_DIR}

echo "Now you can run ${INSTALL_DIR}/activate.sh"

