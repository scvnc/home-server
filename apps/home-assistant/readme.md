
Launch this service by doing

    docker-compose up -d

and then it should should be served http://localhost:8123

Additionally, the configuration/database will be in `./data/config`

## Updating

Backup first...

    docker-compose pull
    docker-compose down
    docker-compose up   

## TODO
 
TODO: backup plan for `./config`
TODO: prune old images